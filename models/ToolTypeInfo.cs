﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eco.Gameplay.Players;

namespace NextFood.models
{
    public class ToolTypeInfo
    {
        public Player player { get; }

        public ToolTypeInfo(Player player)
        {
            this.player = player;
        }
    }
}
