﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Eco.Gameplay.Items;
using Eco.Gameplay.Systems.Tooltip;
using Eco.Shared.Utils;
using Harmony;
using NextFood.Components;
using NextFood.models;

namespace NextFood.injector
{
    [HarmonyPatch]
    public class FoodItemOverlay
    {

        public static System.Reflection.MethodBase TargetMethod()
        {
            return typeof(FoodItem).GetMethod("FoodTooltip", BindingFlags.Instance | BindingFlags.Public);
        }

        public static void Postfix(FoodItem __instance, ref string __result)
        {
            ToolTypeInfo info = null;
            try
            {
                info = ToolTypeComponent.cachedToolTypeInfo[__instance];
                if (info == null)
                {
                    NextFood.printMessage($"Failed to retreive FoodItem with type {__instance.GetType()}");
                    return;
                }

            }
            catch (KeyNotFoundException e)
            {

            }
            catch (Exception e )
            {
                NextFood.printMessage("Unknow error on food" + e.StackTrace);
            }
      
            var newScore = FoodCalculatorComponent.getSkillPointsVariation(info.player, __instance);
            string variationString = newScore > 0 ? Text.Color(Color.Green, $"+ {Math.Round(newScore,2)} skpts") : Text.Color(Color.Red, $" {Math.Round(newScore, 2)} skpts");
            variationString = Text.Bold(variationString);
            variationString = Text.Size(1.4f, variationString);

            List<string> injectedLines = new List<string>()
            {
                "",
                "",
                Text.Size(2f,Text.ColorUnity(Color.Red.UInt,"=== NextFood plugin ===")),
                Text.Italics(Text.ColorUnity(Color.Yellow.UInt,$"Report generated for user {info.player.DisplayName}")),
                "",
                $"Eat this food will provide you {variationString} /day"
            };

            __result += string.Join("\n", injectedLines);

            ToolTypeComponent.cachedToolTypeInfo.Remove(__instance);
        }

    }
}
